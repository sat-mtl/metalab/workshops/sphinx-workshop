# SOME DESCRIPTIVE TITLE.
# Copyright (C) 2021, Metalab
# This file is distributed under the same license as the Template Docs package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: Template Docs \n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-03-02 11:13-0500\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../../source/index.rst:9
msgid "Contents:"
msgstr ""

#: ../../source/index.rst:7
msgid "Welcome to Template Docs's documentation!"
msgstr ""

#: ../../source/index.rst:16
msgid "Indices and tables"
msgstr ""

#: ../../source/index.rst:18
msgid ":ref:`genindex`"
msgstr ""

#: ../../source/index.rst:19
msgid ":ref:`modindex`"
msgstr ""

#: ../../source/index.rst:20
msgid ":ref:`search`"
msgstr ""
